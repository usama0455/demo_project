import 'package:flutter/material.dart';
import 'package:demo_project/services/authentication.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geocoder/model.dart';
import 'dart:async';



class HomePage extends StatefulWidget {
  HomePage({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  @override
  State<StatefulWidget> createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {

  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  final user_data = FirebaseDatabase.instance.reference();

  final _textEditingController = TextEditingController();
  StreamSubscription<Event> _onTodoAddedSubscription;
  StreamSubscription<Event> _onTodoChangedSubscription;
  String _name = '';
  String _job = '';
  String _recieved ='Tap View to see data';
//   final FirebaseApp app = FirebaseApp(
//  options: FirebaseOptions(
//     googleAppID: "1:311147008288:android:79ade56ab0c83060188745",
//     apiKey: "AIzaSyClfmpK0UXFPDI1nLCFRr8nMXIS6xOnD4g",
//     databaseURL: "https://fir-project-87e6f.firebaseio.com"
//     )
// );

  Query _todoQuery;

  //bool _isEmailVerified = false;

  @override
  void initState() {
    super.initState();
     

  }




  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }

  
void createRecord(){
  user_data.child("1").set({
    'Name': _name,
    'Job': _job
  });
}

void deleteData(){
  user_data.child('1').remove();
}

void getData(){
  user_data.once().then((DataSnapshot snapshot) {
    print('Data : ${snapshot.value}');

    setState(() {
      _recieved =snapshot.value.toString();
    });
  });
}
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Welcome'),
          backgroundColor: Colors.blue[900],
          actions: <Widget>[
            new FlatButton(
                child: new Text('Logout',
                    style: new TextStyle(fontSize: 17.0, color: Colors.white)),
                onPressed: signOut)
          ],
        ),
        body: Stack(
          children: <Widget>[
             _background(),
             _userData(),
             _viewData(),
              _buttons(),
               
            

          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
          },
          tooltip: 'Increment',
          child: Icon(Icons.add),
        )
        
        );
  }
    Widget _background() {
    return new Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        );
  }
  
    Widget _userData(){
          return Container(

                padding: const EdgeInsets.fromLTRB(20.0, 50.0, 20.0, 0.0),
            child: Column(
                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new TextFormField(
                    maxLines: 1,
                    keyboardType: TextInputType.text,
                    autofocus: false,
                      onChanged: (text) {
                          _name = text;
                        },
                    decoration: new InputDecoration(
                        hintText: 'Name',
                    ),
                  ),
                    new TextFormField(
                    maxLines: 1,
                    keyboardType: TextInputType.text,
                    autofocus: false,
                      onChanged: (text) {
                      _job = text;
                    },
                    decoration: new InputDecoration(
                        hintText: 'Job',
                    ),
                  ),
                ],
        )
          );
   
    }

        Widget _viewData(){
          return Container(

                padding: const EdgeInsets.fromLTRB(20.0, 250.0, 20.0, 0.0),
            child: Column(
                // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new Text(
                            _recieved,
                            style: TextStyle(fontFamily: 'SFUID-Medium', fontSize: 16.0, color: Colors.black),
                            textAlign: TextAlign.justify,
                          ),
                   
                ],
        )
          );
   
    }

    Widget _buttons() {
    return new Positioned(
              top: 380,
              left: 160,
              child: Column(

                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  new RaisedButton(
                    padding: const EdgeInsets.all(8.0),
                    textColor: Colors.white,
                    color: Colors.blue,
                    onPressed: (){
                        createRecord();
                    } ,
                    child: new Text("Create"),
                  ),
                  new RaisedButton(
                    padding: const EdgeInsets.all(8.0),
                    textColor: Colors.white,
                    color: Colors.blue,
                    onPressed: (){
                      getData();
                    } ,
                    child: new Text("View"),
                  ),
                  new RaisedButton(
                    padding: const EdgeInsets.all(8.0),
                    textColor: Colors.white,
                    color: Colors.blue,
                    onPressed: (){

                    } ,
                    child: new Text("Update"),
                  ),
                  new RaisedButton(
                   onPressed: (){
                      deleteData();
                    } ,
                    textColor: Colors.white,
                    color: Colors.blue,
                    padding: const EdgeInsets.all(8.0),
                    
                    child: new Text(
                      "Delete",
                    ),
                  ),
                ],
        ),
    );

  }
  


}